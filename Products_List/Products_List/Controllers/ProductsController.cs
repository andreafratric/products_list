﻿using Microsoft.AspNetCore.Mvc;
using Products_List.Entity;

namespace Products_List.Controllers
{
    /// <summary>
    /// ProductsController
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    public class ProductsController : Controller
    {
        /// <summary>
        /// The repository
        /// </summary>
        public readonly IRepository _repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProductsController"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        public ProductsController(IRepository repository)
        {
            _repository = repository;
        }
        
        /// <summary>
        /// Gets all products from given source
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var productsList = _repository.GetAll();
            return View(productsList);
        }
       
        /// <summary>
        /// Returns empty Create View
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            return View();
        }
     
        /// <summary>
        /// Creates the specified product.
        /// </summary>
        /// <param name="product">The product.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ProductEntity product)
        {
            if (ModelState.IsValid)
            {
                _repository.Create(product);
                return RedirectToAction(nameof(Index));
            }

            return View(product);
        }

        /// <summary>
        /// Gets the specific product by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public ActionResult Edit(int? id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }
         
            var product = _repository.GetById((int)id);

            if (product == null)
            {
                return NotFound();
            }
            return View(product);
        }
        
        /// <summary>
        /// Edits the specified product.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="product">The product.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, ProductEntity product)
        {
            if (id != product.Id || id == 0)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                _repository.Edit(id, product);
                return RedirectToAction(nameof(Index));
            }
            return View(product);
        }
    }
}