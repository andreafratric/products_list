﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Products_List.Entity
{
    /// <summary>
    /// JsonRepository
    /// </summary>
    /// <seealso cref="Products_List.Entity.IRepository" />
    public class JsonRepository : IRepository
    {
        /// <summary>
        /// Creates the specified product.
        /// </summary>
        /// <param name="product">The product.</param>
        /// <returns>
        /// Identifier of created product
        /// </returns>
        public int Create(ProductEntity product)
        {
            List<ProductEntity> existingProducts = GetAll();
            if (existingProducts == null || !existingProducts.Any())
            {
                product.Id = 1;
            }
            else
            {
                IEnumerable<int> allIds = existingProducts.Select(p => p.Id);
                int maxId = allIds.Max();
                product.Id = maxId + 1;
            }
            existingProducts.Add(product);
            string updatedJson = JsonConvert.SerializeObject(existingProducts);
            File.WriteAllText(Helper.JsonFileName, updatedJson);
            return product.Id;
        }

        /// <summary>
        /// Edits the specified product.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="product">The product.</param>
        public void Edit(int id, ProductEntity product)
        {
            List<ProductEntity> existingProducts = GetAll();

            if (existingProducts == null || !existingProducts.Any())
            {
                return;
            }

            var specificProduct = existingProducts.SingleOrDefault(p => p.Id == id);

            if (specificProduct == null)
            {
                return;
            }
            specificProduct.NameProduct = product.NameProduct;
            specificProduct.DescriptionProduct = product.DescriptionProduct;
            specificProduct.Category = product.Category;
            specificProduct.Manufacturer = product.Manufacturer;
            specificProduct.Supplier = product.Supplier;
            specificProduct.Price = product.Price;
            string updatedJson = JsonConvert.SerializeObject(existingProducts);
            File.WriteAllText(Helper.JsonFileName, updatedJson);
        }

        /// <summary>
        /// Gets all products.
        /// </summary>
        /// <returns></returns>
        public List<ProductEntity> GetAll()
        {
            List<ProductEntity> products = ReadJsonFile();
            if (products == null)
            {
                products = new List<ProductEntity>();
            }
            return products;
        }

        /// <summary>
        /// Gets the specific product by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public ProductEntity GetById(int id)
        {
            List<ProductEntity> existingProducts = ReadJsonFile();
            ProductEntity specificProduct = existingProducts.SingleOrDefault(p => p.Id == id);

            if (specificProduct == null)
            {
                return null;
            }
            return specificProduct;
        }

        /// <summary>
        /// Reads the json file.
        /// </summary>
        /// <returns></returns>
        private List<ProductEntity> ReadJsonFile()
        {
            List<ProductEntity> products = null;

            string path = Path.GetFullPath("products.json");

            if (!File.Exists(path))
            {
                var myFile = File.Create(path);
                myFile.Close();
            }

            using (StreamReader sr = new StreamReader(Helper.JsonFileName))
            {
                string json = sr.ReadToEnd();
                products = JsonConvert.DeserializeObject<List<ProductEntity>>(json);
            }
            return products;
        }
    }
}
