﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Products_List.Entity;
using Microsoft.EntityFrameworkCore;
using static Products_List.Enum.Enum;

namespace Products_List
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        private void AddScopeDataSource(IServiceCollection services)
        {
            var dataSource = Configuration.GetSection("DataSource");
            if (dataSource != null && !string.IsNullOrEmpty(dataSource.Value))
            {
                if (dataSource.Value == DataSource.Base.ToString())
                {
                    services.AddScoped<IRepository, EntityRepository>();

                }
                else if (dataSource.Value == DataSource.JSON.ToString())
                {
                    services.AddScoped<IRepository, JsonRepository>();
                }
            }
            else
            {
                services.AddScoped<IRepository, EntityRepository>();
            }
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ProductsContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            AddScopeDataSource(services);
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Products}/{action=Index}/{id?}");
            });
        }
    }
}
