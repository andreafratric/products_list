﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Products_List.Models
{
    /// <summary>
    /// Product
    /// </summary>
    public class Product
    {
        public int Id { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        [Required]
        [Display(Name = "Naziv")]
        public string Name { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public string Manufacturer { get; set; }
        public string Supplier { get; set; }
        public double Price { get; set; }
    }
}
