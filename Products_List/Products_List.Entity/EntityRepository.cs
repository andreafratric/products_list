﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Products_List.Entity
{
    /// <summary>
    /// EntityRepository
    /// </summary>
    /// <seealso cref="Products_List.Entity.IRepository" />
    public class EntityRepository : IRepository
    {
        /// <summary>
        /// The ProductsContext
        /// </summary>
        private readonly ProductsContext _context;

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityRepository"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public EntityRepository(ProductsContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Creates the specified product.
        /// </summary>
        /// <param name="product">The product.</param>
        /// <returns>
        /// Identifier of created product.
        /// </returns>
        public int Create(ProductEntity product)
        {
            _context.Add(product);
            _context.SaveChanges();
            return product.Id;
        }

        /// <summary>
        /// Edits the specified product.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="product">The product.</param>
        public void Edit(int id, ProductEntity product)
        {
            _context.Update(product);
            _context.SaveChanges();
        }

        /// <summary>
        /// Gets all products.
        /// </summary>
        /// <returns></returns>
        public List<ProductEntity> GetAll()
        {
            var products = _context.Product.ToList();
            return products;
        }

        /// <summary>
        /// Gets the specific product by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public ProductEntity GetById(int id)
        {
            ProductEntity specificProduct = _context.Product.SingleOrDefault(p => p.Id == id);

            if (specificProduct == null)
            {
                return null;
            }
            return specificProduct;
        }
    }
}
