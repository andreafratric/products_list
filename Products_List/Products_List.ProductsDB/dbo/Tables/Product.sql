﻿CREATE TABLE [dbo].[Product] (
    [Id]                 INT             IDENTITY (1, 1) NOT NULL,
    [NameProduct]        NVARCHAR (100)  NOT NULL,
    [DescriptionProduct] NVARCHAR (100)  NOT NULL,
    [Category]           NVARCHAR (100)  NOT NULL,
    [Manufacturer]       NVARCHAR (100)  NOT NULL,
    [Supplier]           NVARCHAR (100)  NOT NULL,
    [Price]              DECIMAL (10, 2) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

