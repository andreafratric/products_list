﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Products_List.Entity
{
    public partial class ProductsContext : DbContext
    {
        public virtual DbSet<ProductEntity> Product { get; set; }

        public ProductsContext(DbContextOptions<ProductsContext> opcije) : base(opcije) { }       

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        { }
    }
}
