﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Products_List.Entity
{
    /// <summary>
    /// ProductEntity
    /// </summary>
    [Table("Product")]
    public partial class ProductEntity
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name product.
        /// </summary>
        /// <value>
        /// The name product.
        /// </value>
        [Required(ErrorMessage = "Unesite naziv proizvoda")]
        [StringLength(100, ErrorMessage = "Maksimalno 100 karaktera")]
        [Display(Name = "Naziv")]
        public string NameProduct { get; set; }

        /// <summary>
        /// Gets or sets the description product.
        /// </summary>
        /// <value>
        /// The description product.
        /// </value>
        [Required(ErrorMessage = "Unesite opis proizvoda")]
        [StringLength(100, ErrorMessage = "Maksimalno 100 karaktera")]
        [Display(Name = "Opis")]
        public string DescriptionProduct { get; set; }

        /// <summary>
        /// Gets or sets the category.
        /// </summary>
        /// <value>
        /// The category.
        /// </value>
        [Required(ErrorMessage = "Unesite kategoriju proizvoda")]
        [StringLength(100, ErrorMessage = "Maksimalno 100 karaktera")]
        [Display(Name = "Kategorija")]
        public string Category { get; set; }

        /// <summary>
        /// Gets or sets the manufacturer.
        /// </summary>
        /// <value>
        /// The manufacturer.
        /// </value>
        [Required(ErrorMessage = "Unesite proizvođača proizvoda")]
        [StringLength(100, ErrorMessage = "Maksimalno 100 karaktera")]
        [Display(Name = "Proizvođač")]
        public string Manufacturer { get; set; }

        /// <summary>
        /// Gets or sets the supplier.
        /// </summary>
        /// <value>
        /// The supplier.
        /// </value>
        [Required(ErrorMessage = "Unesite dobavljača proizvoda")]
        [StringLength(100, ErrorMessage = "Maksimalno 100 karaktera")]
        [Display(Name = "Dobavljač")]
        public string Supplier { get; set; }

        /// <summary>
        /// Gets or sets the price.
        /// </summary>
        /// <value>
        /// The price.
        /// </value>
        [Required(ErrorMessage = "Unesite cenu proizvoda")]
        [Column(TypeName = "decimal(10, 2)")]
        [Display(Name = "Cena")]
        public decimal Price { get; set; }
    }
}
