﻿using System.Collections.Generic;

namespace Products_List.Entity
{
    /// <summary>
    /// IRepository
    /// </summary>
    public interface IRepository
    {
        /// <summary>
        /// Gets all products.
        /// </summary>
        /// <returns></returns>
        List<ProductEntity> GetAll();

        /// <summary>
        /// Gets the specific product by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        ProductEntity GetById(int id);

        /// <summary>
        /// Creates the specified product.
        /// </summary>
        /// <param name="product">The product.</param>
        /// <returns>Identifier of created product</returns>
        int Create(ProductEntity product);

        /// <summary>
        /// Edits the specified product.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="product">The product.</param>     
        void Edit(int id, ProductEntity product);
    }
}
